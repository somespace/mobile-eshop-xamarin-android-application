﻿using MobileShop.Models;
using MvvmHelpers;
using System.Collections.Generic;

namespace MobileShop.Seasons
{
    /// <summary>
    /// Trida dedi od abstraktni tridy metody k aplikaci slev na objednavku a jeji polozky. 
    /// Procentualni sleva objednavky pri nakupu:
    /// <list type="bullet">
    /// <item>
    /// pro zakaznika s 5 a vice existujicimi objednavky, sleva na objednavku 10%
    /// </item>
    /// <item>
    /// pro zakaznika s 10 a vice existujicimi objednavky, sleva na objednavku 20%
    /// </item>
    /// </list>
    /// Pozn.: na polozku nejsou v lete aplikovany slevy
    /// </summary>
    class Summer : Season
    {
        const int smallerOrdersCount = 5;
        const int biggerOrdersCount = 10;

        const int lowerPercentualDiscount = 10;
        const int higherPercentualDiscount = 20;

        const int lowerOrderDiscountStrategyID = 4;
        const int higherOrderDiscountStrategyID = 5;
        const int noOrderDiscountStrategyID = 10;

        public int AssignedStrategyID { get; private set; }

        public Summer()
        {
            AssignedStrategyID = AssignStrategyID();
        }

        private int AssignStrategyID()
        {
            int strategy = higherOrderDiscountStrategyID;
            int ordersCount = App.Database.GetCustomerOrders(Session.CustomerLoggedIn).Count;
            
            if (ordersCount < smallerOrdersCount)
                strategy = noOrderDiscountStrategyID;
            else if (ordersCount < biggerOrdersCount)
                strategy = lowerOrderDiscountStrategyID;

            return strategy;
        }

        public override List<OrderItem> AddOrderItems(ObservableRangeCollection<Product> basketItems, OrderDetail order)
        {
            List<OrderItem> orderItems = new List<OrderItem>();

            foreach (var product in basketItems)
            {
                OrderItem newItem = new OrderItem
                {
                    OrderID = order.OrderID,
                    ProductID = product.ProductID,
                    Quantity = product.Amount,
                    FixedDiscount = NoDiscount,
                    PercentualDiscount = NoDiscount,
                    StrategyID = AssignedStrategyID
                };
                newItem.SetPrice(product.Price);
                orderItems.Add(newItem);
            }

            return orderItems;
        }

        // slevy objednávky
        public override int GetFixedOrderDiscount(ObservableRangeCollection<Product> basketItems = null)
        {
            return NoDiscount;
        }

        public override int GetPercentualOrderDiscount(ObservableRangeCollection<Product> basketItems = null)
        {
            if (AssignedStrategyID == 10)
                return NoDiscount;
            else if (AssignedStrategyID == 4)
                return lowerPercentualDiscount;
            else
                return higherPercentualDiscount;
        }
    }
}
