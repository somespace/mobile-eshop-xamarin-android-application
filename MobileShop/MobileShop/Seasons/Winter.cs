﻿using MobileShop.Models;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MobileShop.Seasons
{
    /// <summary>
    /// Trida dedi od abstraktni tridy metody k aplikaci slev na objednavku a jeji polozky
    /// Prideleny jsou slevy objednavky:
    /// <list type="bullet">
    /// <item>
    /// pro zakaznika s objednavkou nad 1000 Kc = sleva objednavky 10 % + 100 Kč
    /// </item>
    /// <item>
    /// pro zakaznika s objednavkou nad 5000 Kc = sleva objednavky 20 % + 200 Kč
    /// </item>
    /// <item>
    /// pro zakaznika s objednavkou nad 10000 Kc = sleva objednavky 30 % + 500 Kč
    /// </item>
    /// </list>
    /// Pozn.: slevy polozky nejsou v zime aplikovany
    /// </summary>
    class Winter : Season
    {
        const int nullDiscount = 0;
        const int bottomFixedOrderDiscount = 100;
        const int middleFixedOrderDiscount = 200;
        const int topFixedOrderDiscount = 500;

        const int bottomPercOrderDiscount = 10;
        const int middlePercOrderDiscount = 20;
        const int topPercOrderDiscount = 30;

        const int noOrderDiscountStrategyID = 10;
        const int lowerOrderDiscountStrategyID = 7;
        const int middleOrderDiscountStrategyID = 8;
        const int higherOrderDiscountStrategyID = 9;


        readonly int[] fixedDiscounts = { nullDiscount, bottomFixedOrderDiscount, 
            middleFixedOrderDiscount, topFixedOrderDiscount };

        readonly int[] percentualDiscounts = { nullDiscount, bottomPercOrderDiscount,
            middlePercOrderDiscount, topPercOrderDiscount };

        public override List<OrderItem> AddOrderItems(ObservableRangeCollection<Product> basketItems,
            OrderDetail order)
        {
            List<OrderItem> orderItems = new List<OrderItem>();
            int strategyID = 0;

            switch (GetDiscountLevel(BruttoOrderPrice(basketItems)))
            {
                case 0:
                    strategyID = noOrderDiscountStrategyID;
                    break;
                case 1:
                    strategyID = lowerOrderDiscountStrategyID;
                    break;
                case 2:
                    strategyID = middleOrderDiscountStrategyID;
                    break;
                case 3:
                    strategyID = higherOrderDiscountStrategyID;
                    break;
                default:
                    break;
            }

            foreach (var product in basketItems)
            {
                OrderItem newItem = new OrderItem
                {
                    OrderID = order.OrderID,
                    ProductID = product.ProductID,
                    Quantity = product.Amount,
                    FixedDiscount = NoDiscount
                };
                newItem.SetPrice(product.Price);
                newItem.StrategyID = strategyID;
                orderItems.Add(newItem);
            }

            return orderItems;
        }

        private int BruttoOrderPrice(ObservableRangeCollection<Product> basketItems)
        {
            return basketItems.Sum(item => item.Price * item.Amount);
        }

        private int GetDiscountLevel(int bruttoOrderPrice)
        {
            if (bruttoOrderPrice <= 1000)
            {
                return 0;
            }
            else if (bruttoOrderPrice > 1000 && bruttoOrderPrice < 5000)
            {
                return 1;
            }
            else if (bruttoOrderPrice >= 5000 && bruttoOrderPrice < 10000)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }

        // discounts applied on Order
        public override int GetFixedOrderDiscount(ObservableRangeCollection<Product> basketItems)
        {
            int discountLevel = GetDiscountLevel(BruttoOrderPrice(basketItems));
            return fixedDiscounts[discountLevel];
        }
        public override int GetPercentualOrderDiscount(ObservableRangeCollection<Product> basketItems)
        {
            int discountLevel = GetDiscountLevel(BruttoOrderPrice(basketItems));
            return percentualDiscounts[discountLevel];
        }
    }
}
