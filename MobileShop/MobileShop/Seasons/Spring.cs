﻿using MobileShop.Models;
using MvvmHelpers;
using System.Collections.Generic;

namespace MobileShop.Seasons
{
    /// <summary>
    /// Trida dedi od abstraktni tridy metody k aplikaci slev na objednavku a jeji polozky
    /// Jarni strategie:  
    /// Procentualni polozkova sleva pri nakupu:
    /// <list type="bullet">
    /// <item>
    /// nad 3 polozky vcetne 20%
    /// </item>
    /// <item>
    /// nad 8 vcetne 30%
    /// </item>
    /// <item>
    /// nad 12 vcetne 40%
    /// </item>
    /// </list>
    /// Pozn.: Slevy objednavky pro jarni sezonu neplati
    /// </summary>
    class Spring : Season
    {
        const int littleItemQuantity = 3;
        const int middleItemQuantity = 8;
        const int highItemQuantity = 12;

        const int lowItemPercentualDiscount = 20;
        const int middleItemPercentualDiscount = 30;
        const int highItemPercentualDiscount = 40;

        public override List<OrderItem> AddOrderItems(ObservableRangeCollection<Product> basketItems, 
            OrderDetail order)
        {
            List<OrderItem> orderItems = new List<OrderItem>();

            foreach (var product in basketItems)
            {
                OrderItem newItem = new OrderItem
                {
                    OrderID = order.OrderID,
                    ProductID = product.ProductID,
                    Quantity = product.Amount,
                    FixedDiscount = NoDiscount
                };
                newItem.SetPrice(product.Price);

                if (newItem.Quantity < littleItemQuantity)
                {
                    newItem.PercentualDiscount = NoDiscount;
                    newItem.StrategyID = 10;
                }
                else if (newItem.Quantity < middleItemQuantity)
                {
                    newItem.PercentualDiscount = lowItemPercentualDiscount;
                    newItem.StrategyID = 1;
                }
                else if (newItem.Quantity < highItemQuantity)
                {
                    newItem.PercentualDiscount = middleItemPercentualDiscount;
                    newItem.StrategyID = 2;
                }
                else
                {
                    newItem.PercentualDiscount = highItemPercentualDiscount;
                    newItem.StrategyID = 3;
                }
                orderItems.Add(newItem);
            }

            return orderItems;
        }

        // discounts applied on Order
        public override int GetFixedOrderDiscount(ObservableRangeCollection<Product> basketItems = null) 
            => NoDiscount;
        public override int GetPercentualOrderDiscount(ObservableRangeCollection<Product> basketItems = null) 
            => NoDiscount;
    }
}
