﻿using MobileShop.Models;
using MvvmHelpers;
using System.Collections.Generic;

namespace MobileShop.Seasons
{
    /// <summary>
    /// Trida dedi od abstraktni tridy metody k aplikaci slev na objednavku a jeji polozky
    /// Individualni sleva polozky se aplikuje pokud se produkt nachazi v tabulce specialnich produktu v db
    /// Pozn.: slevy objednavky nejsou na podzim aplikovany
    /// </summary>
    class Autumn : Season
    {
        const int noDiscountID = 10;
        const int specialDiscountID = 6;

        public override List<OrderItem> AddOrderItems(ObservableRangeCollection<Product> basketItems,
            OrderDetail order)
        {
            List<OrderItem> orderItems = new List<OrderItem>();

            foreach (var product in basketItems)
            {
                OrderItem newItem = new OrderItem
                {
                    OrderID = order.OrderID,
                    ProductID = product.ProductID,
                    Quantity = product.Amount,
                    FixedDiscount = App.Database.GetSpecialOfferDiscount(product)
                };
                newItem.SetPrice(product.Price);
                newItem.StrategyID = (newItem.FixedDiscount == 0) ? noDiscountID : specialDiscountID;

                orderItems.Add(newItem);
            }

            return orderItems;
        }

        // discounts applied on Order
        public override int GetFixedOrderDiscount(ObservableRangeCollection<Product> basketItems = null) 
            => NoDiscount;
        public override int GetPercentualOrderDiscount(ObservableRangeCollection<Product> basketItems = null)
            => NoDiscount;
    }
}
