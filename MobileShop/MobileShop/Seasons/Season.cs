﻿using MobileShop.Models;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileShop.Seasons
{
    abstract class Season
    {
        // JEN PRO TESTOVACI UCELY - datumy 4 rocnich obdobi 
        public const string springDate = "2020-04-01 20:55:24.5344018";
        public const string summerDate = "2020-06-01 20:55:24.5344018";
        public const string autumnDate = "2020-10-01 20:55:24.5344018";
        public const string winterDate = "2021-02-01 20:55:24.5344018";

        public int NoDiscount { get; } = 0;

        // overriden methods 
        public abstract int GetFixedOrderDiscount(ObservableRangeCollection<Product> basketItems = null);
        public abstract int GetPercentualOrderDiscount(ObservableRangeCollection<Product> basketItems = null);
        public abstract List<OrderItem> AddOrderItems(ObservableRangeCollection<Product> basketItems, OrderDetail order);

        public OrderDetail CreateNewOrder(ObservableRangeCollection<Product> basketItems)
        {
            OrderDetail order = CreateGenericOrder();
            order.FixedDiscount = GetFixedOrderDiscount(basketItems);
            order.PercentualDiscount = GetPercentualOrderDiscount(basketItems);

            // saving order items under order.id
            order.AssignItems(AddOrderItems(basketItems, order));
            // calculate and assign final prices for order
            AssignFinalOrderPrices(order);

            return order;
        }

        // vytvoreni genericke objednavky
        public static OrderDetail CreateGenericOrder()
        {
            OrderDetail genericOrder = new OrderDetail
            {
                CustomerID = Session.CustomerLoggedIn.CustomerID,
                StateID = 0, // vytvorena
                DateTime = DateTime.Now.ToString()
            };

            return genericOrder;
        }

        public static void AssignFinalOrderPrices(OrderDetail order)
        {
            decimal orderItemsDiscount = 0;
            decimal orderItemsFullPrice = 0;

            foreach(OrderItem item in order.OrderItems)
            {
                int itemFullPrice = item.Quantity * item.Price; 
                decimal percItemPrice = itemFullPrice * ((decimal)item.PercentualDiscount / 100);
                decimal itemDiscount = item.FixedDiscount + percItemPrice;

                orderItemsDiscount += itemDiscount;
                orderItemsFullPrice += itemFullPrice;
            }

            decimal orderItemsTotalPrice = orderItemsFullPrice - orderItemsDiscount;

            // calculate and assign total discount and final price to order
            decimal totalDiscount =
                orderItemsDiscount + order.FixedDiscount + orderItemsTotalPrice 
                * ((decimal)order.PercentualDiscount / 100);

            decimal finalPrice = orderItemsFullPrice - totalDiscount;
            
            order.TotalDiscount = (int)Math.Round(totalDiscount, MidpointRounding.AwayFromZero);
            order.FinalPrice = (int)Math.Round(finalPrice, MidpointRounding.AwayFromZero);
        }

        public static Season GetSeason()
        {
            int thisYear = DateTime.Today.Year;
            DateTime springStart = new DateTime(thisYear, 2, 20);
            DateTime summerStart = new DateTime(thisYear, 5, 21);
            DateTime autumnStart = new DateTime(thisYear, 8, 23);
            DateTime winterStart = new DateTime(thisYear, 11, 21);

            // pro testovani jarni slevy zmen: DateTime date = "DateTime.Parse(Order.springDate); misto 'DateTime.Now'"
            DateTime date = DateTime.Now;

            if (date >= springStart && date < summerStart)
            {
                return new Spring();
            }
            else if (date >= summerStart && date < autumnStart)
            {
                return new Summer();
            }
            else if (date >= autumnStart && date < winterStart)
            {
                return new Autumn();
            }
            else
            {
                return new Winter();
            }
        }
    }
}
