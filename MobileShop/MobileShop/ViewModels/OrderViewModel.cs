﻿using MobileShop.Models;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileShop.ViewModels
{
    class OrderViewModel : BaseViewModel
    {
        public ObservableRangeCollection<OrderDetail> Orders { get; private set; }

        public string state;
        public string State
        {
            get => state;
            set
            {
                if (SetProperty(ref state, value))
                    ChangeState();
            }
        }

        public OrderViewModel()
        {
            Orders = new ObservableRangeCollection<OrderDetail>(App.Database.GetOrders());
        }

        void ChangeState()
        {
            var newOrders = new ObservableRangeCollection<OrderDetail>(Orders);
            Orders.ReplaceRange(newOrders);
        }
    }
}
