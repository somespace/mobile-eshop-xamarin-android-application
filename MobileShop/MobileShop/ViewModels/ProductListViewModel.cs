﻿using MobileShop.Models;
using MvvmHelpers;

namespace MobileShop.ViewModels
{
    public class ProductListViewModel : BaseViewModel
    {
        private ObservableRangeCollection<Product> products;
        public ObservableRangeCollection<Product> Products
        {
            get
            {
                return products;
            }
            set
            {
                if (products != value)
                {
                    products = value;
                    OnPropertyChanged();
                }
            }
        }

        public ProductListViewModel()
        {
            Products = new ObservableRangeCollection<Product>(App.Database.GetProducts());
        }
    }
}
