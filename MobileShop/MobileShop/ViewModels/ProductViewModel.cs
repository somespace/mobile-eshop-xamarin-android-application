﻿using MvvmHelpers;
using System;
using MobileShop.Models;
using Xamarin.Forms;
using System.Linq;

namespace MobileShop.ViewModels
{
    public class ProductViewModel : BaseViewModel
    {
        public ObservableRangeCollection<Product> Products { get; }
        public ObservableRangeCollection<Product> AllProducts { get; set; }
        public ObservableRangeCollection<string> CathegoryOptions { get; }
        public ObservableRangeCollection<string> PriceOptions { get; }

        public bool IsBusy { get; set; }

        private const string all = "Všechny produkty";
        private const string ascending = "Od nejnižší ceny";

        string selectedCathegory = all;
        string selectedPrice = ascending;

        public string SelectedCathegory
        {
            get => selectedCathegory;
            set 
            {
                if (SetProperty(ref selectedCathegory, value))
                    FilterItems();
            }
        }

        public string SelectedPrice
        {
            get => selectedPrice;
            set
            {
                if (SetProperty(ref selectedPrice, value))
                    FilterItems();
            }
        }

        Command LoadProducts { get; set; }

        public ProductViewModel()
        {
            Products = new ObservableRangeCollection<Product>();
            AllProducts = new ObservableRangeCollection<Product>();

            CathegoryOptions = new ObservableRangeCollection<string>
            {
                "Všechny produkty",
                "Chytré hodinky",
                "Nabíječky a kabely",
                "Pouzdra a kryty",
                "Smartfóny",
                "Tablety",
                "Tlačítkové telefony",
                "Tvrzená skla"
            };

            PriceOptions = new ObservableRangeCollection<string>
            {
                "Od nejnižší ceny",
                "Od nejvyšší ceny"
            };

            LoadProducts = new Command(() => ExecuteLoadItemsCommand());
            LoadProducts.Execute(null);
        }

        void ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                var items = App.Database.GetProducts();
                AllProducts.ReplaceRange(items);
                FilterItems();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        void FilterItems()
        {
            int filterCathegoryID = CathegoryOptions.IndexOf(selectedCathegory) - 1;
            
            if(selectedPrice == ascending)
            {
                Products.
                ReplaceRange(AllProducts.
                OrderBy(p => p.Price).
                Where(p =>
                p.CathegoryID == filterCathegoryID || SelectedCathegory == all));
            }
            else
            {
                Products.
                ReplaceRange(AllProducts.
                OrderByDescending(p => p.Price).
                Where(p =>
                p.CathegoryID == filterCathegoryID || SelectedCathegory == all));
            }
        }
    }
}
