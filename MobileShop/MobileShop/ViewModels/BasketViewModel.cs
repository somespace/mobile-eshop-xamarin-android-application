﻿using MobileShop.Models;
using MvvmHelpers;
using System.Collections.Specialized;

namespace MobileShop.ViewModels
{
    public class BasketViewModel : BaseViewModel
    {
        private bool emptyBasketLabelVisibility;
        public bool EmptyBasketLabelVisibility 
        {
            get
            {
                return emptyBasketLabelVisibility;
            }
            private set
            {
                if (emptyBasketLabelVisibility != value)
                {
                    emptyBasketLabelVisibility = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool basketListViewVisibility;
        public bool BasketListViewVisibility 
        { 
            get
            {
                return basketListViewVisibility;
            }
            private set
            {
                if (basketListViewVisibility != value)
                {
                    basketListViewVisibility = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool orderButtonVisibility;
        public bool OrderButtonVisibility
        {
            get
            {
                return orderButtonVisibility;
            }
            private set
            {
                if (orderButtonVisibility != value)
                {
                    orderButtonVisibility = value;
                    OnPropertyChanged();
                }
            }
        }

        private ObservableRangeCollection<Product> basketProducts;
        public ObservableRangeCollection<Product> BasketProducts
        {
            get
            {
                return basketProducts;
            }
            set
            {
                if (basketProducts != value)
                {
                    basketProducts = value;
                    OnPropertyChanged();
                }
            }
        }

        // viewmodel constructor
        public BasketViewModel()
        {
            EmptyBasketLabelVisibility = true;
            BasketListViewVisibility = false;
            OrderButtonVisibility = false;

            BasketProducts = new ObservableRangeCollection<Product>();
            BasketProducts.CollectionChanged += BasketProducts_CollectionChanged;
        }

        private void BasketProducts_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (BasketProducts.Count == 1)
                    {
                        EmptyBasketLabelVisibility = false;
                        BasketListViewVisibility = true;
                        OrderButtonVisibility = true;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    if (BasketProducts.Count == 0)
                    {
                        EmptyBasketLabelVisibility = true;
                        BasketListViewVisibility = false;
                        OrderButtonVisibility = false;
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    EmptyBasketLabelVisibility = true;
                    BasketListViewVisibility = false;
                    OrderButtonVisibility = false;
                    break;
                default:
                    break;
            }
        }
    }
}
