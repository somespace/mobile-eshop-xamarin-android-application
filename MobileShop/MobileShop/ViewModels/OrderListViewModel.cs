﻿using MobileShop.Models;
using MvvmHelpers;

namespace MobileShop.ViewModels
{
    public class OrderListViewModel : BaseViewModel
    {
        private ObservableRangeCollection<OrderDetail> orders;
        public ObservableRangeCollection<OrderDetail> Orders
        {
            get
            {
                return orders;
            }
            set
            {
                if (orders != value)
                {
                    orders = value;
                    OnPropertyChanged();
                }
            }
        }

        public OrderListViewModel()
        {
            Orders = new ObservableRangeCollection<OrderDetail>(App.Database.GetOrders());
        }
    }
}
