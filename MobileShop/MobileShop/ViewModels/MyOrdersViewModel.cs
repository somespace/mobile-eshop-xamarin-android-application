﻿using MobileShop.Models;
using MvvmHelpers;
using System.Collections.Specialized;

namespace MobileShop.ViewModels
{
    public class MyOrdersViewModel : BaseViewModel
    {
        private ObservableRangeCollection<OrderDetail> orders;
        public ObservableRangeCollection<OrderDetail> Orders
        {
            get
            {
                return orders;
            }
            set
            {
                if (orders != value)
                {
                    orders = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool emptyOrdersLabelVisibility;
        public bool EmptyOrdersLabelVisibility
        {
            get
            {
                return emptyOrdersLabelVisibility;
            }
            private set
            {
                if (emptyOrdersLabelVisibility != value)
                {
                    emptyOrdersLabelVisibility = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool myOrdersListViewVisibility;
        public bool MyOrdersListViewVisibility
        {
            get
            {
                return myOrdersListViewVisibility;
            }
            private set
            {
                if (myOrdersListViewVisibility != value)
                {
                    myOrdersListViewVisibility = value;
                    OnPropertyChanged();
                }
            }
        }

        public MyOrdersViewModel()
        {
            EmptyOrdersLabelVisibility = true;
            MyOrdersListViewVisibility = false;
            var myOrders = App.Database.GetCustomerOrders(Session.CustomerLoggedIn);
            Orders = new ObservableRangeCollection<OrderDetail>(myOrders);
            Orders.CollectionChanged += Orders_CollectionChanged;
            
            if (Orders.Count != 0)
            {
                EmptyOrdersLabelVisibility = false;
                MyOrdersListViewVisibility = true;
            }
        }

        private void Orders_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (Orders.Count == 1)
                    {
                        EmptyOrdersLabelVisibility = false;
                        MyOrdersListViewVisibility = true;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
