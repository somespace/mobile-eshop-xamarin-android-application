﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using Xamarin.Forms;

namespace MobileShop
{
    public partial class App : Application
    {
        private const string dbName = "mobileshop.db";
        
        static Database database;

        public static Database Database
        {
            get
            {
                if (database == null)
                {
                    database = new Database(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), dbName));
                    Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.Personal));
                }
                return database;
            }
        }

        public App()
        {
            CopyEmbeddedDatabase();
            InitializeComponent();

            MainPage = new MainPage();
        }

        // copy the db file to the destination of Personal folder 
        void CopyEmbeddedDatabase()
        {
            var personalFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            var destinationPath = Path.Combine(personalFolderPath, dbName);

            using (var source = Assembly.
                GetExecutingAssembly().
                GetManifestResourceStream("MobileShop.AppData.mobileshop.db"))
            {
                if (!File.Exists(destinationPath))
                {
                    using (var destination = File.Create(destinationPath))
                    {
                        source.CopyTo(destination);
                    }
                }
            }
        }


        protected override void OnStart()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("cs-CS");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
