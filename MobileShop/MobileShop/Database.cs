﻿using SQLite;
using System;
using System.Collections.Generic;
using MobileShop.Models;

namespace MobileShop
{
    public class Database
    {
        readonly SQLiteConnection _database;

        public Database(string dbPath)
        {
            _database = new SQLiteConnection(dbPath);
        }

        // vraci kolekci produktu z databaze
        public List<Product> GetProducts()
        {
            var products = _database.Table<Product>().ToList();
            products.Reverse();
            return products;
        }

        public List<OrderDetail> GetOrders()
        {
            var orders = _database.Table<OrderDetail>().ToList();
            orders.Reverse();
            AssingPropertiesToOrders(orders);
            return orders;
        }

        public void DeleteProduct(Product product)
        {
            _database.Delete(product);
        }

        // vraci kolekci zakazniku z databaze
        public List<Customer> GetCustomersAsync()
        {
            return _database.Table<Customer>().ToList();
        }
        
        public Customer GetCustomerByID(int customerID)
        {
            return _database.Table<Customer>().First(c => c.CustomerID == customerID);
        }

        public List<OrderDetail> GetCustomerOrders(Customer customer)
        {
            List<OrderDetail> orders = _database.Table<OrderDetail>()
                .Where(o => o.CustomerID == customer.CustomerID).ToList();
            orders.Reverse();

            AssingPropertiesToOrders(orders);
            return orders;
        }

        public void SaveCustomer(Customer customer)
        {
            _database.Insert(customer);
        }

        private void AssingPropertiesToOrders(List<OrderDetail> orders)
        {
            foreach (OrderDetail order in orders)
                AssignPropertiesToOrder(order);
        }

        public void AssignPropertiesToOrder(OrderDetail order)
        {
            order.StateText = GetOrderStateByID(order.StateID).Description;
            order.Date = Convert.ToDateTime(order.DateTime).ToShortDateString();
            order.Time = Convert.ToDateTime(order.DateTime).ToShortTimeString();
        }

        public List<OrderItem> GetOrderItems(OrderDetail order)
        {
            return _database.Table<OrderItem>().
                Where(i => i.OrderID == order.OrderID).ToList();
        }

        public void SaveOrderAndItems(OrderDetail order)
        {
            _database.Insert(order);
            int orderID = _database.Table<OrderDetail>().
                First(o => o.OrderID == order.OrderID).OrderID;

            foreach (OrderItem item in order.OrderItems)
                item.OrderID = orderID;

            _database.InsertAll(order.OrderItems);
        }

        public void UpdateOrder(OrderDetail order)
        {
            _database.Update(order);
        }

        public void DeleteOrder(OrderDetail order)
        {
            _database.Query<OrderItem>("delete from OrderItem where OrderID = ?", order.OrderID);
            _database.Delete(order);
        }

        public Cathegory GetCathegoryByID(int cathegoryID)
        {
            return _database.Table<Cathegory>().First(c => c.CathegoryID == cathegoryID);
        }

        public OrderState GetOrderStateByID(int stateID)
        {
            return _database.Table<OrderState>().First(s => s.StateID == stateID);
        }

        public Product GetProductFromOrderItem(OrderItem orderItem)
        {
            return _database.Table<Product>().First(p => p.ProductID == orderItem.ProductID);
        }

        public int SaveProduct(Product product)
        {
            _database.Insert(product);
            return _database.Table<Product>().
                First(p => p.Name == product.Name && p.Price == product.Price).ProductID;
        }

        public void UpdateProduct(Product product)
        {
            _database.Update(product);
        }

        public int GetSpecialOfferDiscount(Product product)
        {
            var specialOffer = 
                _database.Table<SpecialOffer>().
                FirstOrDefault(so => so.ProductID == product.ProductID);
            
            if (specialOffer == null)
                return 0;
            else
                return specialOffer.FixedDiscount;
        }

        public void AssignPropertiesToItems(List<OrderItem> orderItems)
        {
            foreach(OrderItem item in orderItems)
            {
                var product = _database.Table<Product>().
                    First(p => p.ProductID == item.ProductID);
                item.ProductName = product.Name;
                item.SetPrice(product.Price);
            }
        }
    }
}
