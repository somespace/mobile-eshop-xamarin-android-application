﻿using System;
using System.Collections.Generic;
using System.Text;
using MobileShop.Models;
using MvvmHelpers;

namespace MobileShop
{
    abstract class Session
    {
        public static Customer CustomerLoggedIn { get; private set; }
        public static bool AdminLoggedIn { get; private set; }

        public static Customer LoginCustomer(List<Customer> customers, string email, string password)
        {
            foreach(Customer customer in customers)
            {
                if (customer.Email == email && customer.Password == password)
                {
                    CustomerLoggedIn = customer;
                    return customer;
                }
            }

            return null;
        }
    }
}
