﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MobileShop.Models
{
    public class OrderItem
    {
        [PrimaryKey, AutoIncrement]
        public int OrderItemID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public int FixedDiscount { get; set; }
        public int PercentualDiscount { get; set; }
        public int StrategyID { get; set; }
        public int OrderID { get; set; }

        [Ignore]
        public int Price { get; private set; }
        
        [Ignore]
        public string ProductName { get; set; }

        public void SetPrice(int price) => Price = price;
    }
}
