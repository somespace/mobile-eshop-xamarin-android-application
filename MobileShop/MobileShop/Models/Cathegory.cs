﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MobileShop.Models
{
    public class Cathegory
    {
        [PrimaryKey, AutoIncrement]
        public int CathegoryID { get; set; }
        public string Description { get; set; }
    }
}
