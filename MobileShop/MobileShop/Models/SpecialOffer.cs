﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MobileShop.Models
{
    public class SpecialOffer
    {
        [PrimaryKey, AutoIncrement]
        public int ProductID { get; set; }
        public int FixedDiscount { get; set; }
    }
}
