﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SQLite;
using Xamarin.Forms;

namespace MobileShop.Models
{
    public class Product
    {
        [PrimaryKey, AutoIncrement]
        public int ProductID { get; set; }
        public string Name { get; set; }
        public int CathegoryID { get; set; }
        public int Price { get; set; }

        private byte[] photo;
        public byte[] Photo 
        { 
            get 
            { 
                return photo; 
            } 
            set 
            { 
                photo = value; 
                PhotoSource = ImageSource.FromStream(() => new MemoryStream(Photo));
            }
        }

        public string Description { get; set; }

        [Ignore]
        public ImageSource PhotoSource { get; private set; }
        
        [Ignore]
        public int Amount { get; set; }

        public Product()
        {
            PhotoSource = ImageSource.FromStream(() => new MemoryStream(Photo));
        }
    }
}
