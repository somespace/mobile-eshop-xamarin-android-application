﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MobileShop.Models
{
    public class Customer
    {
        [PrimaryKey, AutoIncrement]
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Phone { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
