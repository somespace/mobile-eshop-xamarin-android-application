﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MobileShop.Models
{
    public class Strategy
    {
        [PrimaryKey, AutoIncrement]
        public int StrategyID { get; set; }
        public string Description { get; set; }
    }
}
