﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SQLite;

namespace MobileShop.Models
{
    public class OrderDetail
    {
        [PrimaryKey, AutoIncrement]
        public int OrderID { get; set; }
        public int CustomerID { get; set; }
        public int FixedDiscount { get; set; }
        public int PercentualDiscount { get; set; }
        public int StateID { get; set; }
        public string DateTime { get; set; }
        public int FinalPrice { get; set; }
        public int TotalDiscount { get; set; }

        [Ignore]
        public string StateText { get; set; }
        
        [Ignore]
        public string Date { get; set; }
        
        [Ignore]
        public string Time { get; set; }
        
        [Ignore]
        public List<OrderItem> OrderItems { get; private set; }

        public void AssignItems(List<OrderItem> items) => OrderItems = items;
    }
}
