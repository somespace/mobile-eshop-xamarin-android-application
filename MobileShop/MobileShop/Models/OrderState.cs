﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace MobileShop.Models
{
    
    public class OrderState
    {
        [PrimaryKey, AutoIncrement]
        public int StateID { get; set; }
        public string Description { get; set; }
    }
}
