﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomerPage : TabbedPage
    {
        public CustomerPage()
        {
            InitializeComponent();
            Children.Add(new LoginPage());
        }
    }
}