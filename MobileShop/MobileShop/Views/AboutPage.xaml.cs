﻿using System.IO;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public static string licenceContent;
        public AboutPage()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MobileShop.Licence.licence.txt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    licenceContent = reader.ReadToEnd();
                }
            }
            InitializeComponent();
        }
    }
}