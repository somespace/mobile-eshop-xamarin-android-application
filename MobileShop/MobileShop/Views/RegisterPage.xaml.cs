﻿using MobileShop.Models;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void CheckDetailsValidity(out bool errorPresent)
        {
            errorPresent = false;

            string errorMessage = "Tyto údaje jsou vyplněny chybně: \n\n";

            List<Entry> entriesToCheck = new List<Entry>()
            {
                nameEntry, lastNameEntry, phoneEntry,
                cityEntry, streetEntry, houseNumberEntry,
                postalCodeEntry, emailEntry, passwordEntry
            };

            List<string> entryNames = new List<string>()
            {
                "- Jméno\n", "- Příjmění\n", "- Telefon\n",
                "- Město\n", "- Ulice\n", "- Číslo\n", "- PSČ\n",
                "- Email\n", "- Heslo\n"
            };

            foreach (var entry in entriesToCheck)
            {
                if (entry.Text.Length == 0)
                {
                    errorPresent = true;
                    errorMessage += entryNames[entriesToCheck.IndexOf(entry)];
                }
            }
            if (!int.TryParse(phoneEntry.Text, out int _))
            {
                errorPresent = true;
                errorMessage += entryNames[2];
            }

            if (errorPresent)
            {
                DisplayAlert("Chybné údaje", errorMessage, "OK");
            }
        }

        private async void RegisterCustomerBtn_Clicked(object sender, EventArgs e)
        {
            bool errorInDetails = false;
            CheckDetailsValidity(out errorInDetails);

            if (errorInDetails) 
                return;

            Customer newCustomer = new Customer
            {
                Name = nameEntry.Text,
                LastName = lastNameEntry.Text,
                Phone = int.Parse(phoneEntry.Text),
                City = cityEntry.Text,
                Street = streetEntry.Text,
                HouseNumber = houseNumberEntry.Text,
                PostalCode = postalCodeEntry.Text,
                Email = emailEntry.Text,
                Password = passwordEntry.Text
            };

            App.Database.SaveCustomer(newCustomer);

            await DisplayAlert("Registrace dokončena", "Gratulujeme! Váš účet byl úspěšně vytvořen.", "OK");
            await Navigation.PopModalAsync();
        }
    }
}