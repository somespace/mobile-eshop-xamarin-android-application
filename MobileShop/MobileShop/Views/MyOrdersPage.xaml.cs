﻿using MobileShop.Models;
using MobileShop.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyOrdersPage : ContentPage
    {
        public MyOrdersViewModel ViewModel { get; }
        public MyOrdersPage()
        {
            ViewModel = new MyOrdersViewModel();
            BindingContext = ViewModel;
            InitializeComponent();
        }

        private async void ordersListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            await Navigation.
                PushModalAsync(
                    new NavigationPage(new OrderDetailPage(Parent as TabbedPage, e.Item as OrderDetail, null))
                );
        }
    }
}