﻿using MobileShop.Models;
using MobileShop.Services;
using MvvmHelpers;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductEditorPage : ContentPage
    {
        public static Product selectedProduct;
        private ProductListPage ProductListParentPage { get; set; }
        public ObservableRangeCollection<string> CathegoryOptions { get; } 

        public ProductEditorPage(ProductListPage pageParent, Product product = null)
        {
            ProductListParentPage = pageParent;

            CathegoryOptions = new ObservableRangeCollection<string>
            {
                "Chytré hodinky",
                "Nabíječky a kabely",
                "Pouzdra a kryty",
                "Smartfóny",
                "Tablety",
                "Tlačítkové telefony",
                "Tvrzená skla"
            };

            InitializeComponent();

            selectedProduct = product;

            if (selectedProduct == null)
            {
                Title = "Nový produkt";
                LoadEmptyProductDetails();    
            }
            else
            {
                Title = "Úprava produktu";
                LoadProductDetails();
            }
        }

        void LoadEmptyProductDetails()
        {
            productNameEntry.Text = string.Empty;
            productCathegoryPicker.ItemsSource = CathegoryOptions;
            productCathegoryPicker.SelectedIndex = -1;
            productPriceEntry.Text = string.Empty;
            productDescriptionEditor.Text = string.Empty;
        }

        void LoadProductDetails()
        {
            if (selectedProduct.Photo != null)
                productImage.Source = selectedProduct.PhotoSource;
            productNameEntry.Text = selectedProduct.Name;
            productCathegoryPicker.ItemsSource = CathegoryOptions;
            productCathegoryPicker.SelectedIndex = selectedProduct.CathegoryID;
            productPriceEntry.Text = selectedProduct.Price.ToString();
            productDescriptionEditor.Text = selectedProduct.Description;
        }

        private void CheckDetailsValidity(out bool errorPresent)
        {
            errorPresent = false;
            string errorMessage = "Tyto údaje jsou vyplněny chybně: \n\n";

            if (productNameEntry.Text.Length == 0)
            {
                errorMessage += "- Název\n";
                errorPresent = true;
            }
            if (productCathegoryPicker.SelectedIndex < 0)
            {
                errorMessage += "- Kategorie\n";
                errorPresent = true;
            }
            if (!int.TryParse(productPriceEntry.Text, out _))
            {
                errorMessage += "- Cena\n";
                errorPresent = true;
            }
            if (productDescriptionEditor.Text.Length == 0)
            {
                errorMessage += "- Popis\n";
                errorPresent = true;
            }
            if (errorPresent)
            {
                DisplayAlert("Chybné údaje", errorMessage, "OK");
            }
        }

        private async void saveChangesButton_Clicked(object sender, System.EventArgs e)
        {
            bool errorInDetails = false;
            CheckDetailsValidity(out errorInDetails);

            if (errorInDetails) 
                return;

            // new product created
            if (selectedProduct == null)
            {
                Product newProduct = new Product();
                AssignPropertiesToProduct(newProduct);
                newProduct.ProductID = App.Database.SaveProduct(newProduct);
                ProductListParentPage.ViewModel.Products.Insert(0, newProduct);
            }
            else // old product is edited
            {
                AssignPropertiesToProduct(selectedProduct);
                App.Database.UpdateProduct(selectedProduct);
                var updatedProducts = 
                    new ObservableRangeCollection<Product>(ProductListParentPage.ViewModel.Products);
                ProductListParentPage.ViewModel.Products.ReplaceRange(updatedProducts);
            }
            
            await DisplayAlert("Produkt uložen", "Údaje produktu byli úspěšně uloženy.", "OK");
            await Navigation.PopModalAsync();
        }

        private void AssignPropertiesToProduct(Product product)
        {
            product.Name = productNameEntry.Text;
            product.CathegoryID = productCathegoryPicker.SelectedIndex;
            product.Price = int.Parse(productPriceEntry.Text);
            product.Description = productDescriptionEditor.Text;
        }

        // clicked event uses DependencyService to call GetImageStreamAsync method
        // that calls platform object and if stream is returned, then ImageSource
        // is assigned to product picture. (on canceling by user null is returned )
        private async void productImage_Clicked(object sender, System.EventArgs e)
        {
            Stream stream = await DependencyService.Get<IPhotoPickerService>().GetImageStreamAsync();

            if (stream != null)
            {
                // productImage.Source = ImageSource.FromStream(() => stream);
                using (MemoryStream ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    selectedProduct.Photo = ms.ToArray();
                }
                productImage.Source = selectedProduct.PhotoSource;
            }
        }
    }
}