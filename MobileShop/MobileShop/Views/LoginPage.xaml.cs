﻿using MobileShop.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private async void OnLoginBtn_Clicked(object sender, EventArgs e)
        {
            var customers = App.Database.GetCustomersAsync();

            Customer loggedIn = Session.LoginCustomer(customers, emailEntry.Text, passwordEntry.Text);
            if (loggedIn != null)
            {
                TabbedPage parent = this.Parent as TabbedPage;
                parent.Title = loggedIn.Name + " " + loggedIn.LastName;
                parent.Children.Remove(this);
                parent.Children.Insert(0, new ShopPage());
                parent.Children.Insert(1, new BasketPage());
                parent.Children.Insert(2, new MyOrdersPage());

                await parent.DisplayAlert("Zákazník přihlášen", string.Format("Vítejte, {0} {1} !", loggedIn.Name, loggedIn.LastName), "OK");
            }
            else
            {
                await DisplayAlert("Špatné údaje", "Zadali jste špatné jméno nebo heslo", "OK");
            }
        }

        private async void OnRegisterBtn_Clicked(object sender, EventArgs e)
        {
            // in order to display navbar the page must be inserted as a new navigation page
            await Navigation.PushModalAsync(new NavigationPage(new RegisterPage()));
        }
    }
}