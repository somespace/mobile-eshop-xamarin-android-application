﻿using MobileShop.Models;
using MobileShop.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductListPage : ContentPage
    {
        public ProductListViewModel ViewModel { get; private set; }
        public ProductListPage()
        {
            ViewModel = new ProductListViewModel();
            BindingContext = ViewModel;
            InitializeComponent();
        }

        private async void productListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            const string detail = "Zobrazit detail";
            const string edit = "Upravit produkt";
            const string remove = "Odstranit produkt";

            Product tappedProduct = e.Item as Product;

            string action = await DisplayActionSheet("Akce produktu", "Zrušiť", null, detail, edit, remove);

            switch (action)
            {
                case detail:
                    // show detail page
                    var productDetailPage = new ProductDetailPage(tappedProduct);
                    await Navigation.PushModalAsync(new NavigationPage(productDetailPage));
                    break;
                case edit:
                    var productEditorPage = new ProductEditorPage(this, tappedProduct);
                    await Navigation.PushModalAsync(new NavigationPage(productEditorPage));
                    break;
                case remove:
                    bool answer = await DisplayAlert("Upozornění", 
                        "Opravdu si přejete odstranit produkt?", "Ano", "Ne");
                    if (answer)
                    {
                        ViewModel.Products.Remove(tappedProduct);
                        App.Database.DeleteProduct(tappedProduct);
                    }
                    break;
                default:
                    break;
            }
        }

        private async void addProductButton_Clicked(object sender, System.EventArgs e)
        {
            addProductButton.Opacity = 0.7;
            var productEditorPage = new ProductEditorPage(this);
            await Navigation.PushModalAsync(new NavigationPage(productEditorPage));
            addProductButton.Opacity = 0.5;
        }
    }
}