﻿using MobileShop.Models;
using MobileShop.ViewModels;
using MvvmHelpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderListPage : ContentPage
    {
        public OrderListViewModel ViewModel { get; private set; }
        public OrderListPage()
        {
            ViewModel = new OrderListViewModel();
            BindingContext = ViewModel;
            InitializeComponent();
        }

        private async void ordersListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            const string confirm = "Potvrdit objednávku";
            const string send = "Odeslat objednávku";
            const string cancel = "Zrušit objednávku";
            const string detail = "Zobrazit detail";
            const string delete = "Odstranit objednávku";

            OrderDetail tappedOrder = e.Item as OrderDetail;

            string action = await DisplayActionSheet("Akce objednávky",
                "Zrušiť", null, confirm, send, cancel, detail, delete);

            switch (action)
            {
                case confirm:
                    tappedOrder.StateID = 1;
                    RefreshOrders(tappedOrder);
                    break;
                case cancel:
                    tappedOrder.StateID = 2;
                    RefreshOrders(tappedOrder);
                    break;
                case send:
                    tappedOrder.StateID = 3;
                    RefreshOrders(tappedOrder);
                    break;
                case delete:
                    bool answer = await DisplayAlert("Upozornění", "Opravdu si přejete odstranit objednávku?", "Ano", "Ne");
                    if (answer)
                    {
                        ViewModel.Orders.Remove(tappedOrder);
                        App.Database.DeleteOrder(tappedOrder);
                    }
                    break;
                case detail:
                    await Navigation.PushModalAsync(
                        new NavigationPage(new OrderDetailPage(null, e.Item as OrderDetail, null)));
                    break;
                default:
                    break;
            }
        }

        private void RefreshOrders(OrderDetail order)
        {
            order.StateText = App.Database.GetOrderStateByID(order.StateID).Description;
            var changedOrders = new ObservableRangeCollection<OrderDetail>(ViewModel.Orders);
            ViewModel.Orders.ReplaceRange(changedOrders);
            App.Database.UpdateOrder(order);
        }
    }
}