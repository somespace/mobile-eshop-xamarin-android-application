﻿using MobileShop.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Specialized;
using MobileShop.ViewModels;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasketPage : ContentPage
    {
        public BasketViewModel ViewModel { get; }

        public BasketPage()
        {
            ViewModel = new BasketViewModel();
            BindingContext = ViewModel;
            InitializeComponent();
        }

        private async void basketListView_ItemTapped_1(object sender, ItemTappedEventArgs e)
        {
            const string detail = "Zobrazit detail";
            const string remove = "Odebrat z košíku";

            string action = await DisplayActionSheet("Položka", "Zrušiť", null, detail, remove);

            switch (action)
            {
                case detail:
                    // show detail page
                    NavigationPage navPage = new NavigationPage(new ProductDetailPage(e.Item as Product));
                    await Navigation.PushModalAsync(navPage);
                    break;
                case remove:
                    // remove product item from basket bin
                    ViewModel.BasketProducts.Remove(e.Item as Product);
                    await DisplayAlert("Položka odebrána", "Vybraná položka byla odebrána z košíku.", "OK");
                    break;
                default:
                    break;
            }
        }

        // open new modal page with order detail
        private async void Button_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new OrderDetailPage(Parent as TabbedPage, null, ViewModel.BasketProducts)));
        }
    }
}