﻿using MobileShop.Models;
using MobileShop.Seasons;
using MvvmHelpers;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderDetailPage : TabbedPage
    {
        private TabbedPage PageParent { get; set; }
        private OrderDetail Order { get; set; }
        private Customer OrderCustomer { get; set; }

        public OrderDetailPage(TabbedPage customerPageParent, OrderDetail order = null,
            ObservableRangeCollection<Product> basketProducts = null)
        {
            PageParent = customerPageParent;
            Order = order;
            InitializeComponent();
            if (basketProducts == null)
            {
                confirmOrderButton.IsVisible = false;
            }
            // nově vytvářená objednávka
            if (Order == null)
            {
                Order = Season.GetSeason().CreateNewOrder(basketProducts);
                LoadOrderData(Session.CustomerLoggedIn, Order);
            }
            else // konkretni objednavka
            {
                LoadCustomerByID();
                LoadOrderData(OrderCustomer, order);
            }
        }

        private void LoadOrderData(Customer customer, OrderDetail order)
        {
            // customer part
            customerName.Text = customer.Name;
            customerLastName.Text = customer.LastName;
            customerPhone.Text = customer.Phone.ToString("### ### ###");
            customerEmail.Text = customer.Email;

            customerCity.Text = customer.City;
            customerStreet.Text = customer.Street;
            customerHouseNumber.Text = customer.HouseNumber;
            customerPostalCode.Text = customer.PostalCode.Substring(0, 3) + " " + customer.PostalCode.Substring(3);

            // if it is old order, we have to recalculate its pricing and assign items to it
            if(order.OrderItems == null)
                order.AssignItems(App.Database.GetOrderItems(order));

            // load order items to list
            App.Database.AssignPropertiesToItems(order.OrderItems);
            orderItemsListView.ItemsSource = order.OrderItems;

            // order part
            fixedOrderDiscount.Text = order.FixedDiscount.ToString("C");
            procOrderDiscount.Text = order.PercentualDiscount.ToString();
            totalOrderDiscount.Text = order.TotalDiscount.ToString("C");
            totalOrderPrice.Text = order.FinalPrice.ToString("C");
        }

        public void LoadCustomerByID()
        {
            var customer = App.Database.GetCustomerByID(Order.CustomerID);
            OrderCustomer = customer;
        }

        private async void orderItemsListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Product product = App.Database.GetProductFromOrderItem(e.Item as OrderItem);
            var productDetailPage = new ProductDetailPage(product);
            await Navigation.PushModalAsync(new NavigationPage(productDetailPage));
        }

        // confirm and save order with items to db, clear basket, focus to my orders
        private async void confirmOrderButton_Clicked_1(object sender, System.EventArgs e)
        {
            App.Database.SaveOrderAndItems(Order);
            var waitingForOK = await DisplayAlert("Objednávka vytvořena",
                            "Vaše objednávka byla úspěšně vytvořena.", null, "OK");

            TabbedPage customerTabbedPage = PageParent;
            BasketPage basket = customerTabbedPage.Children[1] as BasketPage;
            MyOrdersPage myOrders = customerTabbedPage.Children[2] as MyOrdersPage;

            // basket cleared up and order gets into MyOrders on top of the list
            basket.ViewModel.BasketProducts.Clear();
            App.Database.AssignPropertiesToOrder(Order);
            myOrders.ViewModel.Orders.Insert(0, Order);

            // focus on MyOrdersSection
            customerTabbedPage.CurrentPage = myOrders;

            if (!waitingForOK)
                await Navigation.PopModalAsync();
        }
    }
}