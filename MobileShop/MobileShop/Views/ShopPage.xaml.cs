﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MobileShop.Models;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShopPage : ContentPage
    {
        public ShopPage()
        {
            InitializeComponent();
        }

        private async void productListView_ItemTapped_1(object sender, ItemTappedEventArgs e)
        {
            const string detail = "Zobrazit detail";
            const string add = "Přidat do košíku";

            string action = await DisplayActionSheet("Položka", "Zrušiť", null, detail, add);

            switch (action)
            {
                case detail:
                    // show detail page
                    var productDetailPage = new ProductDetailPage(e.Item as Product);
                    await Navigation.PushModalAsync(new NavigationPage(productDetailPage));
                    break;
                case add:
                    // add product item to basket bin
                    string pieces =
                        await DisplayPromptAsync("Počet kusů",
                        "Kolik kusů produktu chcete přidat do košíku?", keyboard: Keyboard.Numeric);

                    if (pieces == null) { return; }

                    int num;
                    if (int.TryParse(pieces, out num) && num != 0)
                    {
                        TabbedPage parent = this.Parent as TabbedPage;
                        BasketPage basketPage = parent.Children[1] as BasketPage;
                        Product prod = e.Item as Product;

                        if (basketPage.ViewModel.BasketProducts.Contains(prod))
                        {
                            int prodIndex = basketPage.ViewModel.BasketProducts.IndexOf(prod);
                            prod.Amount = basketPage.ViewModel.BasketProducts[prodIndex].Amount + num;
                            basketPage.ViewModel.BasketProducts.RemoveAt(prodIndex);
                            basketPage.ViewModel.BasketProducts.Insert(prodIndex, prod);
                        }
                        else
                        {
                            prod.Amount = num;
                            basketPage.ViewModel.BasketProducts.Add(prod);
                        }

                        await DisplayAlert("Položka přidána",
                            "Vybraná položka byla přidána do košíku.", "OK");
                    }
                    else
                    {
                        await DisplayAlert("Špatný počet", "Zkuste to znovu.", "OK");
                    }

                    break;
                default:
                    break;
            }
        }
    }
}