﻿using MobileShop.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductDetailPage : ContentPage
    {
        public static Product SelectedProduct { get; private set; }
        public ProductDetailPage(Product product)
        {
            InitializeComponent();
            SelectedProduct = product;
            productName.Text = SelectedProduct.Name;
            productImage.Source = SelectedProduct.PhotoSource;
            productPrice.Text = SelectedProduct.Price.ToString("C");
            productDescription.Text = SelectedProduct.Description;
            LoadCathegoryName();
        }

        private void LoadCathegoryName()
        {
            var cathegory = App.Database.GetCathegoryByID(SelectedProduct.CathegoryID);
            productCathegory.Text = cathegory.Description;
        }
    }
}