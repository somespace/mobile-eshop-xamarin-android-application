﻿using Xamarin.Forms.Xaml;

namespace MobileShop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdminPage
    {
        public AdminPage()
        {
            Children.Add(new ProductListPage());
            Children.Add(new OrderListPage());
            InitializeComponent();
        }
    }
}