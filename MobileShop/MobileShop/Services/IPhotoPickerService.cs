﻿using System.IO;
using System.Threading.Tasks;

namespace MobileShop.Services
{
    /// <summary>
    /// This interface is implemented using Android-specific code
    /// Task: get image stream from selected picture
    /// </summary>
    public interface IPhotoPickerService
    {
        Task<Stream> GetImageStreamAsync();
    }
}
