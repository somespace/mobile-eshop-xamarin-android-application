﻿using MobileShop.MenuItems;
using MobileShop.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileShop
{
    public partial class MainPage : MasterDetailPage
    {
        private List<MasterPageItem> MenuList { get; set; }
        public MainPage()
        {
            InitializeComponent();

            MenuList = new List<MasterPageItem>();

            // Creating pages for menu navigation
            // Here you can define title for item, 
            // icon on the left side, and page that you want to open after selection
            var page1 = new MasterPageItem() { Title = "Zákazník", Icon = "itemIcon1.png", TargetType = typeof(CustomerPage) };
            var page2 = new MasterPageItem() { Title = "Administrátor", Icon = "itemIcon2.png", TargetType = typeof(AdminPage) };
            var page3 = new MasterPageItem() { Title = "O aplikaci", Icon = "itemIcon3.png", TargetType = typeof(AboutPage) };

            // Adding menu items to menuList
            MenuList.Add(page1);
            MenuList.Add(page2);
            MenuList.Add(page3);

            // Setting our list to be ItemSource for ListView in MainPage.xaml
            navigationDrawerList.ItemsSource = MenuList;

            // Initial navigation, this can be used for our home page
            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(AboutPage))) 
            {
                BarTextColor = Color.White
            };
        }

        // Event for Menu Item selection, here we are going to handle navigation based
        // on user selection in menu ListView
        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;

            Detail = new NavigationPage((Page)Activator.CreateInstance(page))
            {
                BarTextColor = Color.White
            };
            IsPresented = false;
        }
    }
}